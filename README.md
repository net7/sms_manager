# SMS Manager
##### Gestion des SMS simplifiée

## Attention
En cours de développement. **TODO** list :
 * Affichage des messages en plein écran (avec GTK+ par exemple).

## Présentation
_Sms Manager_ est une application pour gérer la réception de SMS, la
 modération et l'affichage à l'écran.
 Elle est écrite en C++.

## Dépendances
_Sms Manager_ utilise différentes librairies:
 *   la librairie [boost](boost.org)
 *   la librairie homemade [libsms](https://git.inpt.fr/net7/libsms)

## Installation

### Installation des dépendances
###### Boost
Sur **ArchLinux**:
```bash
pacman -S boost
```
Sur **Debian**:
```bash
apt install libboost-all-dev
```

###### libsms
Installation depuis les sources :
```bash
git clone 'https://git.inpt.fr/net7/libsms.git' && \
cd libsms/ && \
mkdir build && \
make && \
sudo make install
```
Ceci va installer libsms.so dans votre dossier par défaut de .so,
et va ajouter un dossier sms dans votre dossier par défault d'includes.

### Usage
L'objectif de _Sms Manager_ est de faire simple. Il prend simplement en
argument un fichier de configuration [toml](https://en.wikipedia.org/wiki/TOML).
Voici la structure nécessaire :
```toml
[smsserver]
enabled = true
host = ""
port = 9000

[moderation]
enabled = true
host = ""
port = 9001
```

La section `smsserver` concerne le serveur de **réception** des SMS.
La section `moderation` concerne le serveur de **modération** des SMS.

### Exemples
Par exemple, en reprenant l'exemple ci-dessus, nous configurons _Sms Gateway_
sur l'hôte de la machine qui démarre _Sms Manager_, port _9000_ (ex:
192.168.1.42:9000).
La modération étant active, les modérateurs se netcat sur le port 9001, (
ex: `nc 192.168.1.42:9001`).


