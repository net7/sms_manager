CXX = /usr/bin/clang++
CXXFLAGS = -std=c++17 -Wall -Wextra -Werror -W -pedantic -O2 -g -Iinclude/
LDFLAGS = -Iinclude/ -lsms -lboost_system -lcurl -lpthread -lsfml-graphics -lsfml-window -lsfml-system -fPIC
TARGET=sms_manager

SRCS = src/config.cc src/moderation_server.cc src/sms_manager.cc src/display.cc
OBJS = $(SRCS:.cc=.o)
DEPS = $(SRCS:.cc=.d)

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) ${LDFLAGS} -o $@ $^

$(DEPS): %.d:%.cc
	$(CXX) -MM $(CXXFLAGS) $< > $@

include $(DEPS)

clean:
	rm -rf ${TARGET} ${OBJS} ${DEPS}

