//
// Created by zadig on 10/16/17.
//

#ifndef SMS_MANAGER_CONFIG_HPP
#define SMS_MANAGER_CONFIG_HPP
#include <cpptoml/cpptoml.hpp>
#include <string>

namespace config {

class DisplayConfig {
  public:
    std::string default_font_file = std::string("/usr/share/fonts/noto/NotoSerif-Thin.ttf");
    unsigned int color_n;
    unsigned int color_n_1;
    unsigned int font_size;
    std::string font_file;

    unsigned int window_height;
    unsigned int window_width;
    unsigned int padding_left;
    unsigned int max_column_size;

    explicit DisplayConfig():
            color_n(0xFFFFFFFF),
            color_n_1(0xFFFF00FF),
            font_size(20),
            font_file(default_font_file),
            window_height(768),
            window_width(1366),
            padding_left(10),
            max_column_size(80) {};
};

class no_such_configuration_exception {
    const char *m_errstr;

  public:
    explicit no_such_configuration_exception(const char *reason): m_errstr(reason) {}
    const char *what() { return m_errstr; }
};


class Config {
  private:
    std::shared_ptr<cpptoml::table> m_table;
    const char* m_filepath;
    const char *m_errstr;
  public:
    explicit Config(const char* filepath): m_filepath(filepath), m_errstr("") {}
    bool read_config();
    const char *errstr();

    int get_int(const char *key);
    int get_int(const char *key, int default_value);

    std::string get_string(const char *key);
    std::string get_string(const char *key, std::string& default_value);

    std::vector<std::string> get_string_vector(const char *key);

    bool get_bool(const char *key);
    bool get_bool(const char *key, bool default_value);
};

}


#endif //SMS_MANAGER_CONFIG_HPP
