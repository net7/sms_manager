//
// Created by zadig on 19/11/17.
//

#ifndef SMS_MANAGER_DISPLAY_HPP
#define SMS_MANAGER_DISPLAY_HPP
#include <SFML/Graphics.hpp>
#include <mutex>
#include <sms/sms.hpp>
#include <functional>
#include <config.hpp>

class Text {
  public:
    explicit Text(const std::string& content, unsigned int font_size, sf::Font& font, sf::Color color);
    sf::Text& get_text();
    unsigned int get_height();
  private:
    unsigned int m_height;
    sf::Text m_text;
};

class Sms_manager;

class Display {
    friend class SmsManager;
  public:
    explicit Display(config::DisplayConfig&);
    void set_phone_numbers(std::vector<std::string>& phone_numbers);
    void reload();
    void run();
    void push_sms(std::unique_ptr<sms::Sms> sms);
    void set_close_handler(std::function<void ()> close_handler);

  private:
    void init_window();
    void draw();
    std::mutex m_window_mutex;
    std::mutex m_sms_mutex;
    std::unique_ptr<sf::RenderWindow> m_window;
    sf::Font m_font;
    config::DisplayConfig m_config;
    sf::Color m_color_n;
    sf::Color m_color_n_1;
    std::string m_phone_numbers;
    std::vector<Text> m_sms;
    std::function<void ()> m_close_handler;


};


#endif //SMS_MANAGER_DISPLAY_HPP
