//
// Created by zadig on 10/16/17.
//

#ifndef SMS_MANAGER_SMS_MANAGER_HPP
#define SMS_MANAGER_SMS_MANAGER_HPP
#include <config.hpp>
#include <moderation_server.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#define COUT_DEBUG(x) if (config::CONFIG) std::cout << "[debug] " << x << std::endl

#include <iostream>
#include <sms/server.hpp>
#include <queue>
#include <moderation_server.hpp>
#include <display.hpp>

class SmsManager {
  friend class ModerationServer;
  friend class Display;
  private:
    config::Config m_config;
    const char *m_configuration_filepath;
    std::unique_ptr<ModerationServer> m_moderation_server;
    std::unique_ptr<sms::Server> m_smsserver;
    std::queue<std::unique_ptr<sms::Sms>> m_sms_queue;
    std::unique_ptr<Display> m_display;
    const char *m_errstr;
    std::unique_ptr<std::vector<std::string>> m_phone_numbers;

    void init_smsserver();
    void init_moderation_server();
    void init_display();

    std::unique_ptr<sms::Sms> pop_sms();
    void push_sms_on_moderation(std::unique_ptr<sms::Sms> sms);
    void print(std::unique_ptr<sms::Sms> sms);
    void set_sms_handler();
  public:
    explicit SmsManager(const char *configuration_filepath):
            m_config(configuration_filepath),
            m_configuration_filepath(configuration_filepath){};
    const char* errstr();
    int init();
    void shutdown();
};



#endif //SMS_MANAGER_SMS_MANAGER_HPP
