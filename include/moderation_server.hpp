//
// Created by zadig on 21/10/17.
//

#ifndef SMS_MANAGER_MODERATION_SERVER_HPP
#define SMS_MANAGER_MODERATION_SERVER_HPP
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <sms/sms.hpp>
#include <queue>

#define MAX_RESPONSE_SIZE 128

class Moderator;
class SmsManager;

class ModerationServer {
    friend class Moderator;
    friend class SmsManager;
  private:
    boost::asio::io_service& m_io_service;
    std::unique_ptr<boost::asio::ip::tcp::acceptor> m_acceptor;
    unsigned short m_port;
    std::string m_host;
    std::vector<std::shared_ptr<Moderator>> m_connections;
    SmsManager* m_sms_manager;

    boost::asio::io_service& get_io_service();
    void handle_accept(std::shared_ptr<Moderator> connection, const boost::system::error_code &error);
    void remove_moderator(const Moderator *moderator);
    void validate(std::unique_ptr<sms::Sms> sms);
  public:
    ModerationServer(SmsManager *sms_manager, boost::asio::io_service& io_service, unsigned short port):
            m_io_service(io_service),
            m_port(port),
            m_sms_manager(sms_manager) {};
    void init();
    void start_accept();
    void moderate(std::unique_ptr<sms::Sms> sms);
};

class Moderator {
    friend class ModerationServer;
  private:
    boost::asio::ip::tcp::socket m_socket;
    ModerationServer& m_server;
    char m_buffer[MAX_RESPONSE_SIZE];
    std::queue<std::unique_ptr<sms::Sms>> m_sms_pending;
    std::stringstream m_string_stream;

    void run();
    boost::asio::ip::tcp::socket& get_socket();
    void print_sms(sms::Sms* sms);
    void receive_moderator_decision(const boost::system::error_code &error, size_t bytes_transferred);
    void moderate();
  public:
    explicit Moderator(ModerationServer& server): m_socket(server.get_io_service()),
                                                  m_server(server),
                                                  m_buffer{0} {};
    void moderate(std::unique_ptr<sms::Sms> sms);
};

#endif //SMS_MANAGER_MODERATION_SERVER_HPP
