//
// Created by zadig on 19/11/17.
//

#include <SFML/Graphics.hpp>
#include <mutex>
#include <thread>
#include <iostream>

std::string all = "";
sf::Font font;
sf::Text text;

void window_go(sf::RenderWindow *window, std::mutex* lock) {
  sf::CircleShape shape(100.f);
  shape.setFillColor(sf::Color::Green);
  window->clear();
  window->draw(text);
  window->display();
  while (window->isOpen()) {
    sf::Event event{};
    while (window->waitEvent(event)) {
      switch (event.type) {
        case sf::Event::Closed:
          lock->lock();
          window->close();
          lock->unlock();
          break;
        case sf::Event::Resized:
        case sf::Event::LostFocus:
        case sf::Event::GainedFocus:
          window->clear();
          window->draw(text);
          window->display();
          break;
        default:break;
      }
      if (event.type == sf::Event::Closed) {
        lock->lock();
        window->close();
        lock->unlock();
      }
    }
  }
}

int main() {
  font.loadFromFile("/usr/share/fonts/noto/NotoSerif-Thin.ttf");
  text.setFont(font);
  text.setCharacterSize(24);
  text.setStyle(sf::Text::Regular);
  sf::RenderWindow window(sf::VideoMode(1366, 768), "SFML works!");
  std::mutex lock;
  std::thread t1(window_go, &window, &lock);

  t1.detach();
  std::string txt = "_";
  while (!txt.empty()) {
    std::cout << "getline" << std::endl;
    std::getline(std::cin, txt);
    all += txt + "\n";
    lock.lock();
    text.setString(all);
    window.setTitle(txt.c_str());
    window.clear();
    window.draw(text);
    window.display();
    lock.unlock();
  }
  std::cout << "fin" << std::endl;
  return 0;
}