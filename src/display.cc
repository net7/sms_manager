//
// Created by zadig on 19/11/17.
//

#include <display.hpp>
#include <boost/range/adaptor/reversed.hpp>
#include <boost/container/small_vector.hpp>
#include <iostream>
#include <utility>

void color_to_channels(unsigned int color, unsigned char *channels) {
  /* Red */
  channels[0] = ((unsigned char) (color >> 0x18));
  /* Green */
  channels[1] = ((unsigned char) (color >> 0x10 & 0x00FF));
  /* Blue */
  channels[2] = ((unsigned char) (color >> 0x8 & 0x0000FF));
  /* Alpha */
  channels[3] = ((unsigned char) (color & 0x000000FF));
}

Display::Display(config::DisplayConfig& display_config) {
  m_config = std::move(display_config);
  unsigned char channels[4];
  color_to_channels(display_config.color_n, channels);
  m_color_n.r = channels[0];
  m_color_n.g = channels[1];
  m_color_n.b = channels[2];
  m_color_n.a = channels[3];
  color_to_channels(display_config.color_n_1, channels);
  m_color_n_1.r = channels[0];
  m_color_n_1.g = channels[1];
  m_color_n_1.b = channels[2];
  m_color_n_1.a = channels[3];
  m_font.loadFromFile(m_config.font_file);
  init_window();
}

void Display::set_phone_numbers(std::vector<std::string> &phone_numbers) {
  for(const auto &s : phone_numbers) {
    m_phone_numbers += s + " | ";
  }
  m_phone_numbers = m_phone_numbers.substr(0, m_phone_numbers.size() - 3);
}

void Display::reload() {
  m_window_mutex.lock();
  m_window->clear();
  m_window_mutex.unlock();
  draw();
  m_window_mutex.lock();
  m_window->display();
  m_window_mutex.unlock();
}

void Display::init_window() {
  if (m_config.window_height != 0 && m_config.window_width != 0) {
    m_window = std::make_unique<sf::RenderWindow>(sf::VideoMode(m_config.window_width, m_config.window_height),
                                                  "sms_manager");
  } else {
    m_window = std::make_unique<sf::RenderWindow>(sf::VideoMode::getFullscreenModes()[0], "sms_manager",
                                                  sf::Style::Fullscreen);
  }

}

void Display::run() {
  sf::Event event{};
  m_window->clear();
  reload();
  auto mGUIView = sf::View(sf::FloatRect(0.f, 0.f, m_window->getSize().x, m_window->getSize().y));
  m_window_mutex.lock();
  m_window->display();
  m_window_mutex.unlock();
  while (m_window->isOpen()) {
    while (m_window->waitEvent(event)) {
      switch (event.type) {
        case sf::Event::Closed:
          m_window_mutex.lock();
          m_window->close();
          if (m_close_handler) {
            m_close_handler();
          }
          m_window_mutex.unlock();
          break;
        case sf::Event::Resized:
        case sf::Event::LostFocus:
        case sf::Event::GainedFocus:
          m_config.window_width = m_window->getSize().x;
          m_config.window_height = m_window->getSize().y;
          mGUIView = sf::View(sf::FloatRect(0.f, 0.f, m_config.window_width, m_config.window_height));
          m_window->setView(mGUIView);
          reload();
          break;
        default:break;
      }
    }
  }
}

void Display::draw() {
  unsigned int i = 0;
  unsigned int y = m_config.window_height - (m_config.font_size + 2);
  if (m_window->isOpen()) {
    m_sms_mutex.lock();
    m_window_mutex.lock();
    sf::Text phone_text;
    sf::RectangleShape rectangleShape(sf::Vector2f(m_config.window_width, m_config.font_size * 2));
    rectangleShape.setFillColor(sf::Color(255, 255, 255, 127));
    rectangleShape.setPosition(0, 0);
    phone_text.setString(m_phone_numbers);
    phone_text.setCharacterSize(m_config.font_size);
    phone_text.setFont(m_font);
    phone_text.setStyle(sf::Text::Bold);
    phone_text.setPosition(m_config.font_size / 2, m_config.font_size / 2);
    phone_text.setFillColor(sf::Color::White);
    m_window->draw(phone_text);
    m_window->draw(rectangleShape);
    for(auto &s : boost::adaptors::reverse(m_sms)) {
      if (y > (m_config.font_size * 2)) {
        auto text = s.get_text();
        y -= (text.getLocalBounds().height + (m_config.font_size / 2));
        text.setPosition(m_config.padding_left, y);
        m_window->draw(text);
        i++;
      }
    }
    m_window_mutex.unlock();
    m_sms_mutex.unlock();
  }
}

void split_string(std::string& content, unsigned int max_column_size) {
  for(size_t i = 0; i < content.size(); i++) {
    if (i % max_column_size == 0) {
      content.insert(i, "\n");
    }
  }
}

void Display::push_sms(std::unique_ptr<sms::Sms> sms) {
  m_sms_mutex.lock();
  sf::Color color;
  if (m_sms.size() % 2 == 0) {
    color = m_color_n_1;
  } else {
    color = m_color_n;
  }
  std::string content = sms->get_content();
  /* Check for length */
  split_string(content, m_config.max_column_size);

  Text text(content, m_config.font_size, m_font, color);
  m_sms.push_back(std::move(text));
  m_sms_mutex.unlock();
  reload();
}

void Display::set_close_handler(std::function<void()> close_handler) {
  m_close_handler = std::move(close_handler);
}

Text::Text(const std::string &content, unsigned int font_size, sf::Font& font, sf::Color color) {
  m_text.setCharacterSize(font_size);
  m_text.setFont(font);
  m_text.setFillColor(color);

  auto data = sf::String::fromUtf8(content.begin(), content.end());
  m_text.setString(data);
  m_height = (unsigned int) std::count(content.begin(), content.end(), '\n') + 1;
  if (m_height > 1) {
    m_height += 1;
  }
}

sf::Text& Text::get_text() {
  return m_text;
}

unsigned int Text::get_height() {
  return m_height;
}