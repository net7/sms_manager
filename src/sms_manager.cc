#include <sms/server.hpp>
#include <config.hpp>
#include <iostream>
#include <fstream>
#include <sms_manager.hpp>
#include <thread>


int main(const int argc, const char *argv[]) {
  int ret = 0;
  if (argc < 2) {
    std::cout << "Usage: " << argv[0] << " configuration.toml" << std::endl;
    ret = -1;
  } else {
    SmsManager manager(argv[1]);
    ret = manager.init();
    if (ret != 0) {
      std::cout << manager.errstr() << std::endl;
    }
  }

  return ret;
}

int SmsManager::init() {
  int ret = 0;
  if (m_config.read_config()) {
    this->init_smsserver();
    this->init_moderation_server();
    this->set_sms_handler();
    this->init_display();
    m_smsserver->run();
  } else {
    ret = -1;
    m_errstr = m_config.errstr();
  }
  return ret;
}

void SmsManager::set_sms_handler() {
  std::unique_ptr<sms::Handler> sms_handler = std::make_unique<sms::Handler>();
  sms_handler->m_event = sms::Event::MESSAGE;
  if (m_moderation_server == nullptr) {
    sms_handler->m_callback = std::bind(
            &SmsManager::print,
            this,
            std::placeholders::_1);
  } else {
    sms_handler->m_callback = std::bind(
            &SmsManager::push_sms_on_moderation,
            this,
            std::placeholders::_1);
  }
  m_smsserver->set_event_handler(std::move(sms_handler));
}

void SmsManager::init_smsserver() {
  if (m_config.get_bool("smsserver.enabled", false)) {
    try {
      int port = m_config.get_int("smsserver.port");
      std::string host = m_config.get_string("smsserver.host");
      m_phone_numbers = std::make_unique<std::vector<std::string>>(
              m_config.get_string_vector("smsserver.phone_numbers"));
      if (port > 0 && port < 65535) {
        m_smsserver = std::make_unique<sms::Server>(host, (unsigned short) port);
        m_smsserver->start_accept();
      } else {
        m_errstr = "Invalid port for smsserver.";
      }
    } catch (config::no_such_configuration_exception& e) {
      m_errstr = "Configuration error (variable is missing).";
    }
  }
}

void SmsManager::init_moderation_server() {
  if (m_config.get_bool("moderation.enabled", false)) {
    try {
      int port = m_config.get_int("moderation.port");
      if (port > 0 && port < 65535) {
        m_moderation_server = std::make_unique<ModerationServer>(this, m_smsserver->get_io_service(),
                                                                 (unsigned short) port);
        m_moderation_server->init();
        m_moderation_server->start_accept();
      } else {
        m_errstr = "Invalid port for moderation.";
      }
    } catch (config::no_such_configuration_exception& e) {
      m_errstr = "Configuration error (variable is missing).";
    }
  }

}

const char *SmsManager::errstr() {
  return m_errstr;
}

std::unique_ptr<sms::Sms> SmsManager::pop_sms() {
  std::unique_ptr<sms::Sms> ret = std::move(m_sms_queue.front());
  m_sms_queue.pop();
  return ret;
}

void SmsManager::push_sms_on_moderation(std::unique_ptr<sms::Sms> sms) {
  if (!m_moderation_server->m_connections.empty()) {
    m_moderation_server->moderate(std::move(sms)); // TOCTOU ici ?
  } else {
    m_sms_queue.push(std::move(sms));
  }
}

void SmsManager::print(std::unique_ptr<sms::Sms> sms) {
  m_display->push_sms(std::move(sms));
}

unsigned int hex_to_int(std::string& hex, unsigned int default_value) {
  unsigned int color;
  try {
    color = (unsigned int) std::stoul(hex.substr(1), nullptr, 16);
  } catch (const std::exception& e) {
    color = default_value;
  }

  return color;
}

void SmsManager::init_display() {
  sf::Font font;
  config::DisplayConfig display_config;

  display_config.font_size = (unsigned int) m_config.get_int("display_font_size", display_config.font_size);
  display_config.window_height = (unsigned int) m_config.get_int("display.window_height", display_config.window_height);
  display_config.window_width = (unsigned int) m_config.get_int("display.window_width", display_config.window_width);
  if (m_config.get_bool("display.fullscreen", false)) {
    display_config.window_height = display_config.window_width = 0;
  }
  display_config.font_file = m_config.get_string("display.font_file", display_config.default_font_file);

  std::string default_color = "#FFFFFFFF";
  default_color = m_config.get_string("display.font_color_n", default_color);
  display_config.color_n = hex_to_int(default_color, 0xffffffff);

  default_color = "#FFFF00FF";
  default_color = m_config.get_string("display.font_color_n_1", default_color);
  display_config.color_n_1 = hex_to_int(default_color, 0xFFFF00FF);

  display_config.padding_left = (unsigned int) m_config.get_int("display.padding_left", display_config.padding_left);

  display_config.max_column_size = (unsigned int) m_config.get_int("display.max_column_size",
                                                                   display_config.max_column_size);
  m_display = std::make_unique<Display>(display_config);
  auto phone_numbers = std::move(m_phone_numbers);
  m_display->set_phone_numbers(*phone_numbers);
  m_display->set_close_handler(std::bind(&SmsManager::shutdown, this));
  std::thread t1(std::bind(&Display::run, m_display.get()));
  t1.detach();
}

void SmsManager::shutdown() {
  m_smsserver->get_io_service().stop();

}
