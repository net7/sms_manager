//
// Created by zadig on 10/16/17.
//

#include <config.hpp>
#include <iostream>
#include <sms_manager.hpp>


namespace config {

bool Config::read_config() {
  bool ret = true;
  try {
    m_table = cpptoml::parse_file(m_filepath);
  } catch (cpptoml::parse_exception &e) {
    m_errstr = e.what();
    ret = false;
  }

  return ret;

}

const char *Config::errstr() {
  return m_errstr;
}

int Config::get_int(const char *key) {
  int ret;

  auto val = m_table->get_qualified_as<int>(key);
  if (val) {
    ret = *val;
  } else {
    throw no_such_configuration_exception(key);
  }

  return ret;
}

int Config::get_int(const char *key, int default_value) {
  int ret = default_value;
  try {
    ret = get_int(key);
  } catch (no_such_configuration_exception &e) {
    ret = default_value;
  }

  return ret;
}

std::string Config::get_string(const char *key) {
  std::string ret;
  auto val = m_table->get_qualified_as<std::string>(key);
  if (val) {
    ret = *val;
  } else {
    throw no_such_configuration_exception(key);
  }

  return ret;
}

std::string Config::get_string(const char *key, std::string &default_value) {
  std::string ret;
  try {
    ret = get_string(key);
  } catch (no_such_configuration_exception &e) {
    ret = default_value;
  }

  return ret;
}

bool Config::get_bool(const char *key) {
  bool ret;
  auto val = m_table->get_qualified_as<bool>(key);
  if (val) {
    ret = *val;
  } else {
    throw no_such_configuration_exception(key);
  }

  return ret;
}

bool Config::get_bool(const char *key, bool default_value) {
  bool ret;
  try {
    ret = get_bool(key);
  } catch (no_such_configuration_exception &e) {
    ret = default_value;
  }

  return ret;
}

std::vector<std::string> Config::get_string_vector(const char *key) {
  std::vector<std::string> ret;
  auto vals = m_table->get_qualified_array_of<std::string>(key);
  if (vals) {
    ret = *vals;
  } else {
    throw no_such_configuration_exception(key);
  }
  return ret;
}

}
