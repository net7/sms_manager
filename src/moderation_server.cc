//
// Created by zadig on 21/10/17.
//

#include <iostream>
#include "moderation_server.hpp"
#include <sms_manager.hpp>

void ModerationServer::init() {
  m_acceptor = std::make_unique<boost::asio::ip::tcp::acceptor>(
          m_io_service,
          boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), m_port));
}

void ModerationServer::handle_accept(std::shared_ptr<Moderator> connection, const boost::system::error_code &error) {
  if (!error) {
    m_connections.push_back(connection);
    connection->run();
  }
  this->start_accept();
}

void ModerationServer::start_accept() {
  auto connection = std::make_shared<Moderator>(*this);
  m_acceptor->async_accept(connection->get_socket(),
                           std::bind(&ModerationServer::handle_accept, this, connection,
                                     std::placeholders::_1));
}

boost::asio::io_service& ModerationServer::get_io_service() {
  return m_io_service;
}

void ModerationServer::validate(std::unique_ptr<sms::Sms> sms) {
  m_sms_manager->print(std::move(sms));
}

void ModerationServer::remove_moderator(const Moderator *moderator) {
  auto a = m_connections.begin();
  while (a != m_connections.end() && a->get() != moderator) {
    a++;
  }
  if (a != m_connections.end()) {
    m_connections.erase(a);
  }
}

void ModerationServer::moderate(std::unique_ptr<sms::Sms> sms) {
  auto i = std::rand() % m_connections.size();
  auto moderator = m_connections.at(i);
  moderator->moderate(std::move(sms));
}


void Moderator::run() {
  m_socket.async_read_some(
          boost::asio::buffer(m_buffer, MAX_RESPONSE_SIZE),
          std::bind(&Moderator::receive_moderator_decision, this, std::placeholders::_1, std::placeholders::_2)
  );
}

void Moderator::receive_moderator_decision(const boost::system::error_code &error, size_t bytes_transferred) {
  if (!error && bytes_transferred > 0) {
    if (!m_sms_pending.empty()) {
      if ( (m_buffer[0] == 'o' || m_buffer[0] == 'O' || m_buffer[0] == '\n' || m_buffer[0] == '\x0d')
                                                                             && bytes_transferred <= 2) {
        auto sms = std::move(m_sms_pending.front());
        m_server.validate(std::move(sms));
        m_sms_pending.pop();
        moderate();
      } else if (m_buffer[0] != 'n' && m_buffer[0] != 'N' && bytes_transferred > 2) {
        auto sms = m_sms_pending.front().get();
        m_socket.send(boost::asio::buffer("Invalid answer. Please answer O or n.\n"));
        print_sms(sms);
      } else {
        auto sms = std::move(m_sms_pending.front());
        m_sms_pending.pop();
        moderate();
      }
    }
    std::fill(m_buffer, m_buffer + MAX_RESPONSE_SIZE, 0);
    this->run();
  } else {
    m_server.remove_moderator(this);
  }
}

boost::asio::ip::tcp::socket &Moderator::get_socket() {
  return m_socket;
}

void Moderator::moderate(std::unique_ptr<sms::Sms> sms) {
  sms::Sms *to_print = sms.get();
  m_sms_pending.push(std::move(sms));
  if (m_sms_pending.size() == 1) {
    print_sms(to_print);
  }
}

void Moderator::moderate() {
  if (!m_sms_pending.empty()) {
    print_sms(m_sms_pending.front().get());
  }
}

void Moderator::print_sms(sms::Sms* sms) {
  m_string_stream << '<' << sms->get_number() << "> " << sms->get_content() << " [O/n] ";
  m_socket.send(boost::asio::buffer(m_string_stream.str()));
  m_string_stream.str(std::string());
  m_string_stream.clear();
}